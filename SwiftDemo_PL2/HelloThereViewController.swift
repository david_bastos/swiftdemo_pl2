//
//  HelloThereViewController.swift
//  SwiftDemo_PL2
//
//  Created by formando on 10/11/15.
//  Copyright © 2015 David Guilherme Machado Dinis De Bastos. All rights reserved.
//

import UIKit

class HelloThereViewController: UIViewController {
    
    var person:Person?

    @IBOutlet weak var label: UILabel!
    
    @IBOutlet weak var nameField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        if let pname = person{
            label.text = pname.fullName()
        }
        // Do any additional setup after loading the view.
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    
    @IBAction func ChangeNameAction(sender: AnyObject) {
        person?.firstName = nameField.text
        label.text = person?.firstName
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
