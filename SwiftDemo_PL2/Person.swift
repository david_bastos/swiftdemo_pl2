//
//  Person.swift
//  SwiftDemo_PL2
//
//  Created by David Guilherme Machado Dinis De Bastos on 09/11/15.
//  Copyright © 2015 David Guilherme Machado Dinis De Bastos. All rights reserved.
//

import Foundation

class Person {
    
    var firstName:String?
    var lastName:String?
    var nationality = "Portuguese"
    
    struct imcMeasures {
        var height:Double = 0.0
        var weight:Double = 0.0
    }
    
    var johnDoeMeasures:imcMeasures = imcMeasures(height: 1.85,weight: 90.5)
    
    init (){
        self.nationality = "Portuguese"
    }
    
    init (firstName:String, lastName:String, nationality:String){
        self.firstName = firstName
        self.lastName = lastName
        self.nationality = nationality
    }
    
    func fullName() -> String{
        if let fName = self.firstName {
            if let lName = self.lastName {
                return fName + " " + lName
            }
        }
        return " "
    }
    
}