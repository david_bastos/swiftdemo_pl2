//
//  ViewController.swift
//  SwiftDemo_PL2
//
//  Created by David Guilherme Machado Dinis De Bastos on 05/11/15.
//  Copyright © 2015 David Guilherme Machado Dinis De Bastos. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    override func viewDidLoad() {
        super.viewDidLoad()
        johnDoe.firstName = "John"
        johnDoe.lastName = "Doe"
        // Do any additional setup after loading the view, typically from a nib.
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    var person = Person()
    @IBOutlet weak var FirstNameField: UITextField!
    @IBOutlet weak var LastNameField: UITextField!
    @IBOutlet weak var NationalityField: UITextField!
    @IBOutlet weak var LabelText: UILabel!
    var names = ["Mary","John","Joe"]
    var ages = [String:Int]()
    var age = 23
    
    var johnDoe = Person()
    var michaelSmith = Person(firstName: "Michael",lastName:"Smith",nationality: "English")
    
    @IBAction func ButtonPressed(sender: UIButton) {
        
        person.firstName = FirstNameField.text
        person.lastName = LastNameField.text
        person.nationality = NationalityField.text ?? "Unknown"
    
        
        for name in names{
            ages[name] = age++
        }
        
        for (name,age) in ages{
            print(name + " is " + String(age) + " years old")
        }
        LabelText.text = "Hello " + person.fullName() + " you are from\(person.nationality)"
        
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let controller = segue.destinationViewController as? HelloThereViewController {
            controller.person = self.person
        }
        
        
    }
    
}